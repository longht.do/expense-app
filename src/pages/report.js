/* eslint-disable */
import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import api from '../lib/api';
import { connect } from 'react-redux';
import { getExpenses, updateExpenses, addToStarredList } from '../state/app';
import Layout from '../components/layout';
import SEO from '../components/seo';
import dateFormat from 'dateformat';
import styles from './report.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import cloneDeep from 'lodash/cloneDeep';

const Report = () => (
	<Layout>
		<SEO title="Report" />
		<h1 className={styles.title}>Report</h1>
		<ConnectedReportView />
	</Layout>
);

class ReportView extends React.Component {
	state = {
		date: new Date()
	};

	handleChange = (date) => {
		this.setState({
			date: date
		});
		const formattedDate = dateFormat(date, 'dd/mm/yyyy');
		api.getExpensesByDate(formattedDate).then((expenses) => {
			this.props.dispatch(getExpenses(expenses));
		});
	};

	componentDidMount() {
		api.getExpenses().then((expenses) => {
			this.props.dispatch(getExpenses(expenses));
		});
	}

	totalExpense() {
		const expenses = this.props.expenses;
		let total = 0;
		for (let i = 0; i < expenses.length; i++) {
			total = total + expenses[i].amount;
		}
		total = (total / 100).toFixed(2);
		return total;
	}

	constructor(props) {
		super(props);

		this.state = {
			showSort: false
		};

		this.toggleSort = this.toggleSort.bind(this);
		this.handleClickCheapest = this.handleClickCheapest.bind(this);
		this.handleClickMostExpensive = this.handleClickMostExpensive.bind(this);
		this.toggleStar = this.toggleStar.bind(this);
	}

	toggleSort() {
		this.setState({
			showSort: !this.state.showSort
		});
	}

	toggleStar(item) {
		this.props.dispatch(addToStarredList(item));
	}

	sortSectionCssClass() {
		if (this.state.showSort) {
			return `${styles.sortSection} ${styles.showSort}`;
		} else {
			return styles.sortSection;
		}
	}

	handleClickCheapest() {
		const expensesSortedByCheapest = cloneDeep(this.props.expenses);
		expensesSortedByCheapest.sort((a, b) => {
			if (a.amount > b.amount) {
				return 1;
			} else if (a.amount < b.amount) {
				return -1;
			} else {
				return 0;
			}
		});
		this.props.dispatch(updateExpenses(expensesSortedByCheapest));

		this.toggleSort();
	}

	handleClickMostExpensive() {
		const expensesSortedByMostExpensive = cloneDeep(this.props.expenses);
		expensesSortedByMostExpensive.sort((a, b) => {
			if (a.amount > b.amount) {
				return -1;
			} else if (a.amount < b.amount) {
				return 1;
			} else {
				return 0;
			}
		});
		this.props.dispatch(updateExpenses(expensesSortedByMostExpensive));
		this.toggleSort();
	}

	render() {
		return (
			<div>
				<label className={styles.label}>Choose a date</label>

				<div>
					<DatePicker
						className={styles.datePicker}
						selected={this.state.date}
						onChange={this.handleChange}
						dateFormat="dd/MM/yyyy"
					/>
				</div>
				<div>
					<div className={styles.expenses}>
						Expenses
						<div className={this.sortSectionCssClass()}>
							<FontAwesomeIcon className={styles.sortButton} icon="sort" onClick={this.toggleSort} />
							<div className={styles.sortContent}>
								<span className={styles.dropdownItems}>
									<p onClick={this.handleClickCheapest}>Cheapest</p>
									<p onClick={this.handleClickMostExpensive}>Most expensive</p>
								</span>
							</div>
						</div>
					</div>
				</div>

				<table className={styles.table}>
					<thead className={styles.tableHead}>
						<tr>
							<th>Starred Items</th>
							<th>Item</th>
							<th>Amount</th>
							<th>Date</th>
						</tr>
					</thead>

					<tbody className={styles.tableBody}>
						{this.props.expenses.map((expense, index) => {
							return (
								<tr key={index}>
									<td>
										<FontAwesomeIcon
											icon="star"
											onClick={() => {
												this.toggleStar(expense);
											}}
										/>
									</td>
									<td>{expense.item}</td>
									<td>${(expense.amount / 100).toFixed(2)}</td>
									<td>{expense.date}</td>
								</tr>
							);
						})}
						<tr>
							<td>TOTAL</td>
							<td>${this.totalExpense()}</td>
							<td />
							<td />
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		expenses: state.app.expenses
	};
};

export const ConnectedReportView = connect(mapStateToProps, null)(ReportView);

export default Report;
