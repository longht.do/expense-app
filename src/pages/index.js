import React, { useState } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import Layout from '../components/layout';
import SEO from '../components/seo';
import styles from './index.module.scss';
import Bought from '../components/bought';
import ToBuy from '../components/toBuy';

const initialData = {
	tobuy: [
		{
			id: "1",
			name: "Cat food"
		},
		{
			id: "2",
			name: "Chilli sauce"
		}
	],
	bought: [
		{
			id: "3",
			name: "Milk"
		},
		{
			id: "4",
			name: "Bread"
		}
	],
};

const ShoppingList = () => {
	const [toBuyList, setToBuyList] = useState(initialData.tobuy);
	const [boughtList, setBoughtList] = useState(initialData.bought);

	const removeBoughtItemHandler = (removedItem) => {
		const newList = boughtList.filter(item => item.name !== removedItem.name)
		setBoughtList(newList);
	}

	const removeToBuyItemHandler = (removedItem) => {
		const newList = toBuyList.filter(item => item.name !== removedItem.name)
		setToBuyList(newList);

		const newBoughtList = boughtList.concat(removedItem);
		setBoughtList(newBoughtList);
	}

	const addToBuyItemHandler = (addedItem) => {
		const newList = toBuyList.concat(addedItem);
		setToBuyList(newList);
	}
		return (
			<div className={styles.container}>
				<div className={styles.toBuyContainer}>
					<h3 className={styles.list}>
						To buy
					</h3>
					<ToBuy className={styles.toBuyList}
						items={toBuyList}
						removeToBuyItemHandler={removeToBuyItemHandler}
						addToBuyItemHandler={addToBuyItemHandler}
					/>
				</div>
				<div className={styles.boughtContainer}>
					<h3 className={styles.list}>
						Bought
					</h3>
					<Bought className={styles.boughtList}
						items={boughtList}
						removeBoughtItemHandler={removeBoughtItemHandler}
					/>
				</div>
			</div>
		)
}

const IndexPage = () =>
  (
		<Layout>
			<SEO title="Home" />
			<h1 className={styles.title}>Shopping list</h1>
			<ShoppingList />
		</Layout>
	)
;

export default IndexPage;
