/* eslint-disable */
import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { connect } from 'react-redux';
import { saveExpense } from '../state/app';
import Layout from '../components/layout';
import SEO from '../components/seo';
import dateFormat from 'dateformat';
import styles from './expense.module.scss';

const ExpensePage = () => (
	<Layout>
		<SEO title="Home" />
		<h1 className={styles.title}>Add an item</h1>
		<ConnectedForm />
	</Layout>
);

class Form extends React.Component {
	state = {
		date: new Date()
	};

	handleChange = (date) => {
		this.setState({
			date: date
		});
	};

	handleSubmit = (event) => {
		event.preventDefault();
		const formattedAmount = Math.round(Number(this.state.amount) * 100);
		const formattedDate = dateFormat(this.state.date, 'dd/mm/yyyy');
		const expense = {
			date: formattedDate,
			amount: formattedAmount,
			item: this.state.item
		};
		this.props.dispatch(saveExpense(expense));
	};

	handleItemChange = (event) => {
		this.setState({ item: event.target.value });
	};

	handleAmountChange = (event) => {
		this.setState({ amount: event.target.value });
	};

	render() {
		return (
			<form className={styles.form} onSubmit={this.handleSubmit}>
				<label>Date</label>
				<br />
				<DatePicker selected={this.state.date} onChange={this.handleChange} dateFormat="dd/MM/yyyy" />
				<br />
				<label>Item</label>
				<br />
				<input type="text" name="item" onChange={this.handleItemChange} />
				<br />
				<label>Amount</label>
				<br />
				<div className={styles.amountInput}>
					<div className={styles.dollarSign}>$</div>
					<input type="number" min="0" step="0.01" name="amount" onChange={this.handleAmountChange} />
				</div>
				<br />
				<div className={styles.buttonContainer}>
					<button className={styles.button} type="submit">
						Add
					</button>
				</div>
			</form>
		);
	}
}

const mapStateToProps = (state) => ({});

const ConnectedForm = connect(mapStateToProps, null)(Form);

export default ExpensePage;
