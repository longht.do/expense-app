import React from 'react';
import { Provider } from 'react-redux';
import { createStore as reduxCreateStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '.';

const createStore = () => reduxCreateStore(rootReducer, applyMiddleware(thunk));
const store = createStore();
if (typeof window === 'undefined') {
	global.window = {};
}
window.store = store;

export default ({ element }) => <Provider store={store}>{element}</Provider>;
