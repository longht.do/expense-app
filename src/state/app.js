import api from '../lib/api';
import cloneDeep from 'lodash/cloneDeep';

const initialState = {
	expenses: [],
	starredItems: []
};

const GET_EXPENSES = 'GET_EXPENSES';
const UPDATE_EXPENSES = 'UPDATE_EXPENSES';
const ADD_TO_STARRED_LIST = 'ADD_TO_STARRED_LIST';

export const saveExpense = (expense) => {
	return (dispatch) => {
		api.postExpense(expense).then((_res) => {
			api.getExpenses().then((expenses) => {
				dispatch(getExpenses(expenses));
			});
		});
	};
};

export const addToStarredList = (item) => {
	return {
		type: ADD_TO_STARRED_LIST,
		payload: item
	};
};

export const getExpenses = (expenses) => {
	return {
		type: GET_EXPENSES,
		payload: expenses
	};
};

export const updateExpenses = (newExpenses) => {
	return {
		type: UPDATE_EXPENSES,
		payload: newExpenses
	};
};

export default (state = initialState, action) => {
	switch (action.type) {
		case GET_EXPENSES:
			return {
				...state,
				...{ expenses: action.payload }
			};
		case UPDATE_EXPENSES:
			return {
				...state,
				...{ expenses: action.payload }
			};
		case ADD_TO_STARRED_LIST:
			const newStarredItems = cloneDeep(state.starredItems);
			newStarredItems.push(action.payload);

			return {
				...state,
				...{ starredItems: newStarredItems }
			};
		default:
			return state;
	}
};
