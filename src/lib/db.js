import firebase from 'firebase/app';
import 'firebase/firestore';

const db = firebase
	.initializeApp({
		apiKey: process.env.FIREBASE_API_KEY || 'AIzaSyDk5dK0Oqq4z5fj582jRaWebBA7rumsygw',
		authDomain: process.env.FIREBASE_AUTH_DOMAIN || 'expense-production-96920.firebaseapp.com',
		databaseURL: process.env.FIREBASE_DATABASE_URL || 'https://expense-production-96920.firebaseio.com',
		projectId: process.env.FIREBASE_PROJECT_ID || 'expense-production-96920',
		storageBucket: process.env.FIREBASE_STORAGE_BUCKET || '',
		messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID || '90804293135',
		appId: process.env.FIREBASE_APP_ID || '1:90804293135:web:5ed51e30f464219e5b1d7a'
	})
	.firestore();

export default db;
