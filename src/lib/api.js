import db from './db';

const transformDoc = (doc) => {
	return {
		date: doc.data().date,
		item: doc.data().item,
		amount: doc.data().amount
	};
};

const filterData = (querySnapshot, date) => {
	const sortByDate = (a, b) => {
		if (a.date > b.date) {
			return -1;
		} else if (a.date < b.date) {
			return 1;
		} else {
			return 0;
		}
	};

	let result = [];
	querySnapshot.forEach((doc) => {
		if (date) {
			if (date === doc.data().date) {
				result.push(transformDoc(doc));
			}
		} else {
			result.push(transformDoc(doc));
		}
	});
	return result.sort(sortByDate);
};

const api = {
	postExpense: (expense) => {
		return new Promise((resolve, reject) => {
			db.collection('expenses').add(expense).then(resolve).catch(reject);
		});
	},
	getExpenses: () => {
		return new Promise((resolve, _reject) => {
			db.collection('expenses').get().then((querySnapshot) => {
				resolve(filterData(querySnapshot));
			});
		});
	},
	getExpensesByDate: (date) => {
		return new Promise((resolve, _reject) => {
			db.collection('expenses').get().then((querySnapshot) => {
				resolve(filterData(querySnapshot, date));
			});
		});
	}
};

export default api;
