import React, { useState } from 'react';
import styles from '../pages/index.module.scss'

const ToBuyItem = ({item, onRemoveHandler}) => {
  return (
    <div>
      <button onClick={ () => { onRemoveHandler(item)} }>
        ✓
      </button>
      <span key={item.id}>{item.name}</span>
			<br /><br />
    </div>
  )
}

const ToBuy = ({ items, removeToBuyItemHandler, addToBuyItemHandler }) => {
  const [itemToAddName, setItemToAddName] = useState('');
  const onChangeHandler = (event) => {
    setItemToAddName(event.target.value)
  }

  const addedItem = {
    id: Math.floor(Math.random() * 10) + 1,
    name: itemToAddName
  }

  return (
    <div>
      <div>
        <input type='text' name='itemToBuy' onChange={onChangeHandler} />
        <button className={styles.button} type='button' onClick={ () => {addToBuyItemHandler(addedItem)}}>
          Add me
        </button>
      </div>
      <ul>
        {
          items.map((item) => (
            <ToBuyItem item={item} onRemoveHandler={removeToBuyItemHandler}/>
          ))
        }
      </ul>
    </div>
  );
};

export default ToBuy;