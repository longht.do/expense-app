import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';
import logo from '../images/logo.png';

const Header = ({ siteTitle }) => (
	<header
		style={{
			background: `rebeccapurple`,
			marginBottom: 0
		}}
	>
		<div
			style={{
				margin: `0 auto`,
				padding: `1.45rem 1.0875rem`,
				display: 'flex'
			}}
		>
			<img src={logo} alt="logo" style={{ height: '44px', marginBottom: 0, marginRight: '16px' }} />

			<h1 style={{ margin: 0 }}>
				<Link
					to="/"
					style={{
						color: `white`,
						textDecoration: `none`
					}}
				>
					{siteTitle}
				</Link>
			</h1>
		</div>
	</header>
);

Header.propTypes = {
	siteTitle: PropTypes.string
};

Header.defaultProps = {
	siteTitle: ``
};

export default Header;
