import { Link } from 'gatsby';
import React from 'react';
import styles from './navbar.module.scss';

const Navbar = () => (
	<div>
		<ul className={styles.navList}>
			<li>
				<Link to="/expense/">Add expense</Link>
			</li>
			<li>
				<Link to="/report/">View Report</Link>
			</li>
		</ul>
	</div>
);

export default Navbar;
