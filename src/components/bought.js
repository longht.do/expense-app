import React from 'react';

const BoughtItem = ({item, onRemoveHandler}) => {
	return (
		<div>
			<button	onClick={() => { onRemoveHandler(item) }}>
				X
			</button>
			<span key={item.id}>{item.name}</span>
			<br /><br />
		</div>
	)
}

const Bought = ({ items, removeBoughtItemHandler }) => {
	return (
		<div>
			<ul>
				{
					items.map((item) => (
						<BoughtItem item={item} onRemoveHandler={removeBoughtItemHandler}/>
					))
				}
			</ul>
		</div>
	)
};

export default Bought;